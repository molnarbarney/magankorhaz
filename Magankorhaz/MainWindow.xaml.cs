﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Magankorhaz
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            osztalyok.Items.Add("Belgyógyászat");
            osztalyok.Items.Add("Sebészet");
            osztalyok.Items.Add("Intenzív");

            ev.Items.Add("1982");
            honap.Items.Add("Január");
            nap.Items.Add("08");

            osztalyok_2.Items.Add("Belgyógyászat");
            szobaszam.Items.Add("5");
        }
    }
}
